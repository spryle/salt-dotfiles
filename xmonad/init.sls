xmonad-pkgs:
  pkg:
    - installed
    - names:
      - xmonad
      - libghc-xmonad-dev
      - libghc-xmonad-contrib-dev
      - xcompmgr
      - ssh-askpass-gnome
      - trayer
      - xbattbar
      - feh

xmonad-badge:
  file.managed:
    - name: /usr/share/unity-greeter/custom_xmonad_badge.png
    - source: salt://xmonad/usr/share/unity-greeter/custom_xmonad_badge.png
    - mode: 0644

xmonad-desktop:
  file.managed:
    - name: /usr/share/xsessions/xmonad.desktop
    - source: salt://xmonad/usr/share/xsessions/xmonad.desktop
    - mode: 0644

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('xmonad:accounts', []) %}
{{ account }}-xmonad.start:
  file.managed:
    - name: {{ data['home'] }}/.xmonad/xmonad.start
    - source: salt://xmonad/home/.xmonad/xmonad.start
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0744
    - template: jinja
    - requires:
      - user: {{ account }}
{% endif %}
{% endfor %}

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('xmonad:accounts', []) %}
{{ account }}-xmonad.exec:
  file.managed:
    - name: {{ data['home'] }}/.xmonad/xmonad.exec
    - source: salt://xmonad/home/.xmonad/xmonad.exec
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0744
    - template: jinja
    - requires:
      - user: {{ account }}
{% endif %}
{% endfor %}
