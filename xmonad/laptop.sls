include:
  - xmonad

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('xmonad:accounts', []) %}
{{ account }}-xmonad-desktop:
  file.managed:
    - name: {{ data['home'] }}/.xmonad/xmonad.hs
    - source: salt://xmonad/home/.xmonad/laptop.hs
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - template: jinja
    - requires:
      - user: {{ account }}
{% endif %}
{% endfor %}
