roxterm:
  pkg.installed:
    - names:
      - roxterm

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('roxterm:accounts', []) %}
{{ account }}-roxterm-config:
  file.recurse:
    - name: {{ data['home'] }}/.config/roxterm.sourceforge.net/
    - source: salt://roxterm/home/.config/roxterm.sourceforge.net/
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - requires:
      - user: {{ account }}
      - pkg: roxterm
{% endif %}
{% endfor %}
