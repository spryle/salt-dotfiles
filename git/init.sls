git:
  pkg.installed:
    - names:
      - git

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('git:accounts', []) %}
{{ account }}-gitconfig:
  file.managed:
    - name: {{ data['home'] }}/.gitconfig
    - source: salt://git/home/.gitconfig
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - template: jinja
    - requires:
      - user: {{ account }}
      - pkg: git

{{ account }}-gitignore:
  file.managed:
    - name: {{ data['home'] }}/.gitignore
    - source: salt://git/home/.gitignore
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - requires:
      - user: {{ account }}
      - pkg: git
{% endif %}
{% endfor %}
