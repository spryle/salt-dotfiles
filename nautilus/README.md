# Nautilus (no-desktop)

nautilus --no-desktop fix

cp /usr/bin/nautilus /usr/bin/nautilus.original
cp /usr/local/bin/nautilus.nodesktop /usr/local/bin/nautilus
