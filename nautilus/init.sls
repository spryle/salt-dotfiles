nautilus-nodesktop:
  file.managed:
    - name: /usr/local/bin/nautilus.nodesktop
    - source: salt://nautilus/usr/local/bin/nautilus.nodesktop
    - user: root
    - group: root
    - mode: 0755
