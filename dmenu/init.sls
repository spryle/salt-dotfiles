dmenu-pkgs:
  pkg.installed:
    - names:
      - suckless-tools

dmenu-pickle:
  file.managed:
    - name: /usr/local/bin/dmenu-pickle
    - source: salt://dmenu/usr/local/bin/dmenu-pickle
    - mode: 0755
    - requires:
      - pkg: dmenu-pkgs


dmenu-go:
  file.managed:
    - name: /usr/local/bin/dmenu-go
    - source: salt://dmenu/usr/local/bin/dmenu-go
    - mode: 0755
    - requires:
      - pkg: dmenu-pkgs



