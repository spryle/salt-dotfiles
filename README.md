# dotfiles

targets Ubuntu 14.04  
powered by [saltstack](http://docs.saltstack.com/en/latest/)

With a salt master using saltstack [gitfs backend](http://docs.saltstack.com/en/latest/topics/tutorials/gitfs.html)

    salt-call state.sls vim

Locally locally using [salt masterless](http://docs.saltstack.com/en/latest/topics/tutorials/quickstart.html)

    salt-call --local state.sls vim
