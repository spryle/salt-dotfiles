vim:
  pkg.installed:
    - names:
      - vim

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('vim:accounts', []) %}
{{ account }}-vim-directory:
  file.directory:
    - name: {{ data['home'] }}/.vim
    - user: {{ account }}
    - group: {{ account }}
    - makedirs: True
    - requires:
      - user: {{ account }}
      - pkg: vim
{% endif %}
{% endfor %}

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('vim:accounts', []) %}
{{ account }}-vimrc:
  file.managed:
    - name: {{ data['home'] }}/.vimrc
    - source: salt://vim/home/.vimrc
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - template: jinja
    - requires:
      - user: {{ account }}
      - file: {{ account }}-vim-directory
      - pkg: vim
{% endif %}
{% endfor %}

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('vim:accounts', []) %}
{{ account }}-vim-folder:
  file.recurse:
    - name: {{ data['home'] }}/.vim
    - source: salt://vim/home/.vim
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - requires:
      - user: {{ account }}
      - pkg: vim
{% endif %}
{% endfor %}

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('vim:accounts', []) %}
{{ account }}-vundle-folder:
  git.latest:
    - name: https://github.com/gmarik/Vundle.git
    - target: {{ data['home'] }}/.vim/bundle/vundle
    - source: salt://vim/home/.vim
    - user: {{ account }}
    - group: {{ account }}
    - rev: master
    - requires:
      - user: {{ account }}-vim-folder
      - pkg: vim
{% endif %}
{% endfor %}
