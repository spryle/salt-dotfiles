system-font-folder:
  file.recurse:
    - name: /usr/share/fonts
    - source: salt://fonts/usr/share/fonts

system-font-conf:
  file.recurse:
    - name: /etc/fonts/conf.d
    - source: salt://fonts/etc/fonts/conf.d

system-fc-cache:
  cmd.run:
    - name: fc-cache -vf
    - requires:
      - file: system-font-folder
