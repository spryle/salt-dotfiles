powerline-packages:
  pip.installed:
    - names:
      - git+https://github.com/Lokaltog/powerline

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('powerline:accounts', []) %}
{{ account }}-powerline-config:
  file.recurse:
    - name: {{ data['home'] }}/.config/powerline/
    - source: salt://powerline/home/.config/powerline/
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - requires:
      - user: {{ account }}
      - pkg: powerline-packages
{% endif %}
{% endfor %}
