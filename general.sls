general-pkgs:
  pkg.installed:
    - names:
      - build-essential
      - linux-headers-{{ salt["cmd.run"]("uname -r") }}
      - python-dev
      - python-pip
      - python-software-properties
      - python-virtualenv

general-python-pkgs:
  pip.installed:
    - names:
      - psutil
      - distribute
      - virtualenv
    - upgrade: True

general-os-pkgs:
  pkg.installed:
    - names:
      - trayer
      - feh
      - unclutter
      - pidgin
      - google-chrome-stable
      - firefox
      - flashplugin-installer
      - pcmanfm
    - upgrade: True
