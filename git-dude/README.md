# git-dude

.bashrc

if [ -z "$(pgrep git-dude)" ]
then
    # Run the dude
    git-dude ~/.git-dude &
else:
    # Do Nothing 
fi
