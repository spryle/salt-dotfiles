bashmarks:
  file.managed:
    - name: /usr/local/bin/bashmarks.sh
    - source: salt://bashmarks/usr/local/bin/bashmarks.sh
    - mode: 0755
    - template: jinja
