nodejs-ppa:
  pkgrepo.managed:
    - humanname: Node PPA
    - name: ppa:chris-lea/node.js
    - require_in:
      - pkg: nodejs
  pkg.latest:
    - name: nodejs
    - refresh: True
