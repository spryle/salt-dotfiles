include:
  - xmobar

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('xmobar:accounts', []) %}
{{ account }}-xmobarrc:
  file.managed:
    - name: {{ data['home'] }}/.xmobarrc
    - source: salt://xmobar/home/.desktop
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - template: jinja
    - requires:
      - user: {{ account }}
      - pkg: xmobar-pkgs
{% endif %}
{% endfor %}
