bash:
  pkg:
    - installed

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('vim:accounts', []) %}
{{ account }}-bashrc:
  file.managed:
    - name: {{ data['home'] }}/.bashrc
    - source: salt://bash/home/.bashrc
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - template: jinja
    - requires:
      - user: {{ account }}
      - pkg: bash

{{ account }}-bash-profile:
  file.managed:
    - name: {{ data['home'] }}/.profile
    - source: salt://bash/home/.profile
    - user: {{ account }}
    - group: {{ account }}
    - env: {{ account }}
    - mode: 0644
    - requires:
      - user: {{ account }}
      - pkg: bash
{% endif %}
{% endfor %}
