tmux:
  pkg:
    - installed

{% for account, data in salt["pillar.get"]("accounts", []).iteritems() %}
{% if account in salt["pillar.get"]("tmux:accounts", []) %}
{{ account }}-tmux.conf:
  file.managed:
    - name: {{ data["home"] }}/.tmux.conf
    - source: salt://tmux/home/.tmux.conf
    - env: {{ account }}
    - user: {{ account }}
    - group: {{ account }}
    - mode: 0644
    - template: jinja
    - require:
      - pkg: tmux
      - user: {{ account }}
{% endif %}
{% endfor %}
