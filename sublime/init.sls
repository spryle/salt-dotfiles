sublime-ppa:
  pkgrepo.managed:
    - humanname: Sublime Text PPA
    - name: ppa:webupd8team/sublime-text-3
    - require_in:
      - pkg: sublime-text-installer
  pkg.latest:
    - name: sublime-text-installer
    - refresh: True

{% for account, data in salt['pillar.get']('accounts', {}).iteritems() %}
{% if account in salt['pillar.get']('sublime:accounts', []) %}
{{ account }}-sublime-user-config:
  file.recurse:
    - name: {{ data['home'] }}/.config/sublime-text-3/Packages/User
    - source: salt://sublime/home/.config/sublime-text-3/Packages/User
    - env: {{ account }}
    - user: {{ account }}
    - group: {{ account }}
    - template: jinja
    - requires:
      - user: {{ account }}
      - pkg: sublime-ppa 
{% endif %}
{% endfor %}


